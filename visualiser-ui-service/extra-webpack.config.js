const webpack = require('webpack');

module.exports = {
    plugins: [ new webpack.DefinePlugin({
        '$ENV': {
            HOST_IP: JSON.stringify(process.env.HOST_IP)
        }
    }) ]
}