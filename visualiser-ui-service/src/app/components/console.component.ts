import { Component, OnInit } from '@angular/core';
import { LoggingService } from '../services/logging.service';
import { Log } from '../interfaces/log.interface';

@Component({
    selector: 'app-console',
    templateUrl: './console.component.html',
    styleUrls: ['./console.component.css']
})
export class ConsoleComponent implements OnInit {

    logs: Log[];

    constructor(private readonly loggingService: LoggingService) {

    }

    ngOnInit(): void {
        this.logs = [];
        this.loggingService.onNewLog().subscribe(log => {
            this.logs.push(log);
        });
    }
}
