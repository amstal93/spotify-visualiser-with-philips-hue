import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HueService } from '../services/hue.service';
import { HueGroup } from '../interfaces/hue.interface';
import { VisualiserService } from '../services/visualiser.service';

@Component({
    selector: 'app-hue-lights',
    templateUrl: './hue-lights.component.html',
    styleUrls: ['./hue-lights.component.css']
})
export class HueLightsComponent implements OnInit {

    config: HueGroup[];
    visualiserInProgress: boolean;

    constructor(private readonly hueService: HueService,
                private readonly visualiserService: VisualiserService) {

    }

    ngOnInit(): void {
        this.visualiserInProgress = false;
        this.visualiserService.onVisualiserInProgressChange().subscribe(inProgress => {
            this.visualiserInProgress = inProgress;
        });
        this.refreshLightsTable();
    }

    refreshLightsTable(): void {
        this.hueService.getAllLightsByRoom().then((groups) => {
            this.config = groups;
            this.updateConfig();
        });
    }

    updateConfig(): void {
        this.visualiserService.setLightConfig(this.config);
    }
}
