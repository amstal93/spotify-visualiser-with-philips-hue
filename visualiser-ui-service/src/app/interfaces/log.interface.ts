import { LogType } from '../enums/log-type.enum';

export interface Log {
    date: Date;
    message: string;
    logType: LogType;
}
