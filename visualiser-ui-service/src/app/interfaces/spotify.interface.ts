export interface Track {
    id: string;
    isPlaying: boolean;
    progress: number;
}

export interface TrackAnalysis {
    tempo: number;
    beats: number[];
}
