export interface VisualisationType {
    id: string;
    displayName: string;
}

export interface VisualiserConfig {
    authentication: {
        hue_bridge_ip: string;
        hue_username: string;
        spotify_access_token: string;
    };
    visualisationType: string;
    lights: number[];
    offset: number;
    brightness: number;
}
