import { Injectable } from '@angular/core';
import { HueService } from './hue.service';
import { HueGroup } from '../interfaces/hue.interface';
import { LoggingService } from './logging.service';
import { Observable, Subject } from 'rxjs';
import { UrlService } from './url.service';
import { CredentialsService } from './credentials.service';
import { SpotifyService } from './spotify.service';
import { Track, TrackAnalysis } from '../interfaces/spotify.interface';
import { VisualiserConfig, VisualisationType } from '../interfaces/visualiser.interface';
import { LogType } from '../enums/log-type.enum';
import { io, Socket } from 'socket.io-client';

@Injectable({
    providedIn: 'root'
})
export class VisualiserService {

    private visualiserInProgress: boolean;
    private visualiserInProgressSubject: Subject<boolean>;

    private visualiserSocket: Socket;

    private currentTrack: Track;
    private currentTrackAudioAnalysis: TrackAnalysis;
    private currentlyPlayingUpdateInterval: NodeJS.Timer;

    private lightConfig: HueGroup[];
    private visualisationType: VisualisationType;
    private offset: number;
    private brightness: number;

    constructor(private readonly hueService: HueService,
                private readonly spotifyService: SpotifyService,
                private readonly urlService: UrlService,
                private readonly loggingService: LoggingService,
                private readonly credentialsService: CredentialsService) {
        this.visualiserInProgress = false;
        this.visualiserInProgressSubject = new Subject<boolean>();
        this.visualiserSocket = null;
    }

    setLightConfig(lightConfig: HueGroup[]): void {
        this.lightConfig = lightConfig;
        this.sendVisualiserConfig();
    }

    setVisualisationType(visualisationType: VisualisationType): void {
        this.visualisationType = visualisationType;
        this.sendVisualiserConfig();
    }

    setOffset(offset: number): void {
        this.offset = offset;
        this.sendVisualiserConfig();
    }

    setBrightness(brightness: number): void {
        this.brightness = brightness;
        this.sendVisualiserConfig();
    }

    startVisualiser(): void {
        if (this.visualiserInProgress) {
            this.loggingService.newLog('Attempted to start visualisation when it is already in progress', LogType.ERROR);
            return;
        }
        if (this.lightConfig.every(group => group.lights.every(light => !light.selected))) {
            this.loggingService.newLog('No lights have been selected for the visualisation', LogType.ERROR);
            return;
        }
        this.visualiserSocket = io(this.urlService.getVisualiserStreamUrl()).connect();
        this.visualiserSocket.on('error', (message: string) => {
            this.loggingService.newLog(message, LogType.ERROR);
        });
        this.sendVisualiserConfig();
        this.currentTrack = {
            id: '',
            isPlaying: false,
            progress: 0
        };
        this.currentlyPlayingUpdateInterval = setInterval(this.updateVisualiser.bind(this), 500);
        this.setVisualiserInProgress(true);
        this.loggingService.newLog('Starting visualisation', LogType.INFO);
    }

    private sendVisualiserConfig(): void {
        if (this.visualiserSocket) {
            const config: VisualiserConfig = {
                authentication: {
                    hue_bridge_ip: this.hueService.getBridgeIp(),
                    hue_username: this.credentialsService.getHueUsername(),
                    spotify_access_token: this.credentialsService.getSpotifyAccessToken()
                },
                visualisationType: this.visualisationType.id,
                lights: [],
                offset: 0,
                brightness: this.brightness
            };
            for (const group of this.lightConfig) {
                for (const light of group.lights) {
                    if (light.selected) {
                        config.lights.push(light.id);
                    }
                }
            }
            this.visualiserSocket.emit('config', config);
        }   
    }

    private updateVisualiser() {
        this.spotifyService.getCurrentlyPlayingTrack().then(async track => {
            if (track) {
                if (!track.isPlaying && this.currentTrack.isPlaying) {
                    this.visualiserSocket.emit('stop', '');
                } else if (track.id !== this.currentTrack.id) {
                    this.visualiserSocket.emit('stop', '');
                    const analysis = await this.spotifyService.getAudioAnalysis(track.id);
                    this.currentTrack = track;
                    this.currentTrack.isPlaying = false;
                    this.currentTrackAudioAnalysis = analysis;
                    this.updateVisualiser();
                } else if (track.isPlaying && !this.currentTrack.isPlaying) {
                    this.visualiserSocket.emit('start', {
                        track_id: track.id,
                        progress: track.progress,
                        audio_analysis: this.currentTrackAudioAnalysis
                    });
                }
                this.currentTrack = track;
            }
        });
        
    }

    stopVisualiser(): void {
        if (!this.visualiserInProgress) {
            this.loggingService.newLog('Attempted to stop visualisation when it is already stopped', LogType.ERROR);
            return;
        }
        this.visualiserSocket.close();
        this.visualiserSocket = null;
        clearInterval(this.currentlyPlayingUpdateInterval);
        this.setVisualiserInProgress(false);
        this.loggingService.newLog('Stopping visualisation', LogType.INFO);
    }

    private setVisualiserInProgress(inProgress: boolean): void {
        this.visualiserInProgress = inProgress;
        this.visualiserInProgressSubject.next(this.visualiserInProgress);
    }

    isVisualiserInProgress(): boolean {
        return this.visualiserInProgress;
    }

    onVisualiserInProgressChange(): Observable<boolean> {
        return this.visualiserInProgressSubject.asObservable();
    }
}
